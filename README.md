# Ratings

> Displays ratings on the TYPO3 Website.

## Usage

- `composer require ruhrconnect/akademie-ruhr-ratings`
- Include Static TypoScript
- Override Template Paths via TypoScript
