#
# Table structure for table 'tx_akademieruhrratings_domain_model_bewertung'
#
CREATE TABLE tx_akademieruhrratings_domain_model_bewertung (
	titel varchar(255) DEFAULT '' NOT NULL,
	lehrer_bewertung int(11) DEFAULT '0' NOT NULL,
	kurs_bewertung int(11) DEFAULT '0' NOT NULL,
	inhalt text,
	autor varchar(255) DEFAULT '' NOT NULL,
	bild_autor int(11) unsigned NOT NULL default '0',
	erstellt_am datetime DEFAULT NULL,
    categories int(11) unsigned DEFAULT '0' NOT NULL,
);
