<?php

$EM_CONF[$_EXTKEY] = [
    'title' => '[Akademie Ruhr] Bewertungen',
    'description' => 'Bewertungen',
    'category' => 'services',
    'author' => 'Pascale Beier',
    'author_email' => 'mail@pascalebeier.de',
    'state' => 'stable',
    'version' => '1.0.10',
    'constraints' => [
        'depends' => [
            'typo3' => '9.5.0-11.5.99',
        ],
        'conflicts' => [],
        'suggests' => [],
    ],
];
