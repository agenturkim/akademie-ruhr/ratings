<?php
namespace RuhrConnect\AkademieRuhrRatings\Domain\Model;

/***
 *
 * This file is part of the "Ratings" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2018 Pascale Beier <p.beier@ruhr-connect.de>, ruhr-connect GmbH
 *           Andreas Wietfeld <a.wietfeld@ruhr-connect.de>, ruhr-connect GmbH
 *
 ***/

/**
 * Rating
 */
class Bewertung extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{
    /**
     * titel
     * 
     * @var string
     */
    protected $titel = '';

    /**
     * lehrerBewertung
     * 
     * @var int
     */
    protected $lehrerBewertung = 0;

    /**
     * kursBewertung
     * 
     * @var int
     */
    protected $kursBewertung = 0;

    /**
     * inhalt
     * 
     * @var string
     */
    protected $inhalt = '';

    /**
     * autor
     * 
     * @var string
     */
    protected $autor = '';

    /**
     * bildAutor
     * 
     * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
     */
    protected $bildAutor = null;

    /**
     * erstelltAm
     * 
     * @var \DateTime
     */
    protected $erstelltAm = null;

    /**
     * Returns the lehrerBewertung
     * 
     * @return int lehrerBewertung
     */
    public function getLehrerBewertung()
    {
        return $this->lehrerBewertung;
    }

    /**
     * Sets the lehrerBewertung
     * 
     * @param int $lehrerBewertung
     * @return void
     */
    public function setLehrerBewertung($lehrerBewertung)
    {
        $this->lehrerBewertung = $lehrerBewertung;
    }

    /**
     * Returns the kursBewertung
     * 
     * @return int kursBewertung
     */
    public function getKursBewertung()
    {
        return $this->kursBewertung;
    }

    /**
     * Sets the kursBewertung
     * 
     * @param int $kursBewertung
     * @return void
     */
    public function setKursBewertung($kursBewertung)
    {
        $this->kursBewertung = $kursBewertung;
    }

    /**
     * Returns the inhalt
     * 
     * @return string inhalt
     */
    public function getInhalt()
    {
        return $this->inhalt;
    }

    /**
     * Sets the inhalt
     * 
     * @param string $inhalt
     * @return void
     */
    public function setInhalt($inhalt)
    {
        $this->inhalt = $inhalt;
    }

    /**
     * Returns the autor
     * 
     * @return string autor
     */
    public function getAutor()
    {
        return $this->autor;
    }

    /**
     * Sets the autor
     * 
     * @param string $autor
     * @return void
     */
    public function setAutor($autor)
    {
        $this->autor = $autor;
    }

    /**
     * Returns the erstelltAm
     * 
     * @return \DateTime erstelltAm
     */
    public function getErstelltAm()
    {
        return $this->erstelltAm;
    }

    /**
     * Sets the erstelltAm
     * 
     * @param \DateTime $erstelltAm
     * @return void
     */
    public function setErstelltAm(\DateTime $erstelltAm)
    {
        $this->erstelltAm = $erstelltAm;
    }

    /**
     * Returns the titel
     * 
     * @return string $titel
     */
    public function getTitel()
    {
        return $this->titel;
    }

    /**
     * Sets the titel
     * 
     * @param string $titel
     * @return void
     */
    public function setTitel($titel)
    {
        $this->titel = $titel;
    }

    /**
     * Returns the bildAutor
     * 
     * @return \TYPO3\CMS\Extbase\Domain\Model\FileReference $bildAutor
     */
    public function getBildAutor()
    {
        return $this->bildAutor;
    }

    /**
     * Sets the bildAutor
     * 
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $bildAutor
     * @return void
     */
    public function setBildAutor(\TYPO3\CMS\Extbase\Domain\Model\FileReference $bildAutor)
    {
        $this->bildAutor = $bildAutor;
    }
}
