<?php
namespace RuhrConnect\AkademieRuhrRatings\Controller;

/***
 *
 * This file is part of the "Ratings" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2018 Pascale Beier <p.beier@ruhr-connect.de>, ruhr-connect GmbH
 *           Andreas Wietfeld <a.wietfeld@ruhr-connect.de>, ruhr-connect GmbH
 *
 ***/

/**
 * BewertungController
 */
class BewertungController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{
    /** @var \RuhrConnect\AkademieRuhrRatings\Domain\Repository\BewertungRepository */
    protected $bewertungRepository;

    /**
     * @param \RuhrConnect\AkademieRuhrRatings\Domain\Repository\BewertungRepository $bewertungRepository
     */
    public function injectBewertungRepository(\RuhrConnect\AkademieRuhrRatings\Domain\Repository\BewertungRepository $bewertungRepository)
    {
        $this->bewertungRepository = $bewertungRepository;
    }

    /**
     * action list
     * 
     * @return void
     */
    public function listAction()
    {
        $ratings = $this->bewertungRepository->findAll();
        $this->view->assign('ratings', $ratings);
    }

    /**
     * action show
     * 
     * @param \RuhrConnect\AkademieRuhrRatings\Domain\Model\Bewertung $bewertung
     * @return void
     */
    public function showAction(\RuhrConnect\AkademieRuhrRatings\Domain\Model\Bewertung $bewertung)
    {
        $this->view->assign('rating', $bewertung);
    }

    /**
     * A special action that is called for "Insert records" only.
     *
     * @return void
     */
    public function showFromRecordsAction()
    {
        $contentObject = $this->configurationManager->getContentObject();
        if ($contentObject->data['uid']) {
            $rating = $this->bewertungRepository->findByUid((int) $contentObject->data['uid']);
            $this->forward('show', NULL, NULL, ['bewertung' => $rating]);
        }
    }

}
