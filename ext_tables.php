<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
            'RuhrConnect.AkademieRuhrRatings',
            'Ratinglist',
            'Bewertungen'
        );

        $pluginSignature = str_replace('_', '', 'akademie_ruhr_ratings') . '_ratinglist';
        $GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist'][$pluginSignature] = 'pi_flexform';
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue($pluginSignature, 'FILE:EXT:akademie_ruhr_ratings/Configuration/FlexForms/flexform_ratinglist.xml');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('akademie_ruhr_ratings', 'Configuration/TypoScript', 'Bewertungen');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_akademieruhrratings_domain_model_bewertung', 'EXT:akademie_ruhr_ratings/Resources/Private/Language/locallang_csh_tx_akademieruhrratings_domain_model_bewertung.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_akademieruhrratings_domain_model_bewertung');

    }
);
