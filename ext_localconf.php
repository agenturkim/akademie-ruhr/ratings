<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'RuhrConnect.AkademieRuhrRatings',
            'Ratinglist',
            [
                'Bewertung' => 'list, show, showFromRecords'
            ],
            // non-cacheable actions
            [
                'Bewertung' => ''
            ]
        );

    // wizards
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
        'mod {
            wizards.newContentElement.wizardItems.plugins {
                elements {
                    ratinglist {
                        iconIdentifier = akademie_ruhr_ratings-plugin-ratinglist
                        title = LLL:EXT:akademie_ruhr_ratings/Resources/Private/Language/locallang_db.xlf:tx_akademie_ruhr_ratings_ratinglist.name
                        description = LLL:EXT:akademie_ruhr_ratings/Resources/Private/Language/locallang_db.xlf:tx_akademie_ruhr_ratings_ratinglist.description
                        tt_content_defValues {
                            CType = list
                            list_type = akademieruhrratings_ratinglist
                        }
                    }
                }
                show = *
            }
       }'
    );
		$iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class);
		
			$iconRegistry->registerIcon(
				'akademie_ruhr_ratings-plugin-ratinglist',
				\TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
				['source' => 'EXT:akademie_ruhr_ratings/Resources/Public/Icons/user_plugin_ratinglist.svg']
			);
		
    }
);
