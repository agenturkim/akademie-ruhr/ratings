<?php
defined('TYPO3_MODE') || die();

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::makeCategorizable(
   'akademie_ruhr_ratings',
   'tx_akademieruhrratings_domain_model_bewertung'
);
