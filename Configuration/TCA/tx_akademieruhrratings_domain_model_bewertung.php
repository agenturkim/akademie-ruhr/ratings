<?php
return [
    'ctrl' => [
        'title' => 'LLL:EXT:akademie_ruhr_ratings/Resources/Private/Language/locallang_db.xlf:tx_akademieruhrratings_domain_model_bewertung',
        'label' => 'titel',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'sortby' => 'sorting',
        'versioningWS' => true,
        'delete' => 'deleted',
        'enablecolumns' => [
            'disabled' => 'hidden',
            'starttime' => 'starttime',
            'endtime' => 'endtime',
        ],
        'searchFields' => 'titel,lehrer_bewertung,kurs_bewertung,inhalt,autor,bild_autor,erstellt_am',
        'iconfile' => 'EXT:akademie_ruhr_ratings/Resources/Public/Icons/tx_akademieruhrratings_domain_model_bewertung.gif'
    ],
    'interface' => [
        'showRecordFieldList' => 'hidden, titel, lehrer_bewertung, kurs_bewertung, inhalt, autor, bild_autor, erstellt_am',
    ],
    'types' => [
        '1' => ['showitem' => 'hidden, titel, lehrer_bewertung, kurs_bewertung, inhalt, autor, bild_autor, erstellt_am, --div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.access, starttime, endtime'],
    ],
    'columns' => [
        't3ver_label' => [
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.versionLabel',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'max' => 255,
            ],
        ],
        'hidden' => [
            'exclude' => true,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.hidden',
            'config' => [
                'type' => 'check',
                'items' => [
                    '1' => [
                        '0' => 'LLL:EXT:lang/locallang_core.xlf:labels.enabled'
                    ]
                ],
            ],
        ],
        'starttime' => [
            'exclude' => true,
            'behaviour' => [
                'allowLanguageSynchronization' => true
            ],
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.starttime',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'size' => 13,
                'eval' => 'datetime',
                'default' => 0,
            ],
        ],
        'endtime' => [
            'exclude' => true,
            'behaviour' => [
                'allowLanguageSynchronization' => true
            ],
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.endtime',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'size' => 13,
                'eval' => 'datetime',
                'default' => 0,
                'range' => [
                    'upper' => mktime(0, 0, 0, 1, 1, 2038)
                ],
            ],
        ],

        'titel' => [
            'exclude' => true,
            'label' => 'LLL:EXT:akademie_ruhr_ratings/Resources/Private/Language/locallang_db.xlf:tx_akademieruhrratings_domain_model_bewertung.titel',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'lehrer_bewertung' => [
            'exclude' => false,
            'label' => 'LLL:EXT:akademie_ruhr_ratings/Resources/Private/Language/locallang_db.xlf:tx_akademieruhrratings_domain_model_bewertung.lehrer_bewertung',
            'config' => [
                'type' => 'input',
                'size' => 4,
                'eval' => 'int,required'
            ]
        ],
        'kurs_bewertung' => [
            'exclude' => false,
            'label' => 'LLL:EXT:akademie_ruhr_ratings/Resources/Private/Language/locallang_db.xlf:tx_akademieruhrratings_domain_model_bewertung.kurs_bewertung',
            'config' => [
                'type' => 'input',
                'size' => 4,
                'eval' => 'int,required'
            ]
        ],
        'inhalt' => [
            'exclude' => false,
            'label' => 'LLL:EXT:akademie_ruhr_ratings/Resources/Private/Language/locallang_db.xlf:tx_akademieruhrratings_domain_model_bewertung.inhalt',
            'config' => [
                'type' => 'text',
                'cols' => 40,
                'rows' => 15,
                'eval' => 'trim,required'
            ]
        ],
        'autor' => [
            'exclude' => false,
            'label' => 'LLL:EXT:akademie_ruhr_ratings/Resources/Private/Language/locallang_db.xlf:tx_akademieruhrratings_domain_model_bewertung.autor',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'bild_autor' => [
            'exclude' => true,
            'label' => 'LLL:EXT:akademie_ruhr_ratings/Resources/Private/Language/locallang_db.xlf:tx_akademieruhrratings_domain_model_bewertung.bild_autor',
            'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig(
                'bild_autor',
                [
                    'appearance' => [
                        'createNewRelationLinkTitle' => 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:images.addFileReference'
                    ],
                    'foreign_types' => [
                        '0' => [
                            'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_TEXT => [
                            'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_IMAGE => [
                            'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_AUDIO => [
                            'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_VIDEO => [
                            'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_APPLICATION => [
                            'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                        ]
                    ],
                    'maxitems' => 1
                ],
                $GLOBALS['TYPO3_CONF_VARS']['GFX']['imagefile_ext']
            ),
        ],
        'erstellt_am' => [
            'exclude' => true,
            'label' => 'LLL:EXT:akademie_ruhr_ratings/Resources/Private/Language/locallang_db.xlf:tx_akademieruhrratings_domain_model_bewertung.erstellt_am',
            'config' => [
                'dbType' => 'datetime',
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'size' => 12,
                'eval' => 'datetime',
                'default' => null,
            ],
        ],
    
    ],
];
